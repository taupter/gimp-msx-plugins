/**
 * utility.h - utility functions for GIMP MSX plugins
 * Copyright (C) 2019 Thies Hecker
 *
 * This file is part of the gimp-msx-plugins.
 *
 * gimp-msx-plugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <libgimp/gimp.h>
#include <stdio.h>
#include <string.h>
#include "types.h"

#ifndef SRC_UTILITY_H_
#define SRC_UTILITY_H_

GimpDrawable* index_image(gint32 image_id, gboolean use_transp, gint dither_mode, gint indexer);
void resize_image(gint32 image_id);
guchar* create_bload_header(guint16 size, guint16 address);
gboolean check_option_combinations(ExportValues plugin_options);

#endif /* SRC_UTILITY_H_ */
