;This file is part of gimp_msx_plugins.
;
;Copyright 2019, Thies Hecker
;
;gimp_msx_plugins is free software: you can redistribute it and/or modify
;it under the terms of the GNU General Public License as published by
;the Free Software Foundation, either version 3 of the License, or
;(at your option) any later version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program.  If not, see <https://www.gnu.org/licenses/>.
;
;this file is to be used with z80asm
;it compiles to a 16k MSX rom image to test the image export
;it sets up VPD registers, loads data into VRAM and displays the image
;Subroutines (BIOS hooks see at the end of code)
;--------------------------------------
;setup assembler config
;--------------------------------------
defs 16384, 0	;make 16k file size (fill 16k with zeros)
;note: address space will be shifted by 16k (4000h)
seek 0000h	;set assembler to start writing data at adress 0000h
org 4000h	;set 4000h to start address in CPU space
;----------------------------------
;define rom image header (16 bytes)
;----------------------------------
;Byte1+2 = ID, Byte3+4 = program start adress, remaining 12 bytes = 0
db 41h, 42h, 10h, 40h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h, 00h
;----------------------------------
;include image data (name tables, color and patter gen.)
;----------------------------------
;pattern tables
seek 0800h
;incbin "Untitled_bwg.xcf.pt1"
seek 1000h
;incbin "Untitled_bwg.xcf.pt2"
seek 1800h
;incbin "Untitled_bwg.xcf.pt3"
;color tables
seek 2000h
;incbin "Untitled_bwg.xcf.ct1"
seek 2800h
;incbin "Untitled_bwg.xcf.ct2"
seek 3000h
;incbin "Untitled_bwg.xcf.ct3"
;name tables
seek 3800h
;incbin "Untitled_bwg.xcf.nt1"
seek 3900h
;incbin "Untitled_bwg.xcf.nt2"
seek 3A00h
;incbin "Untitled_bwg.xcf.nt3"
;-------------------------------
;------------------------------
;start of programm code
seek 0010h
org 4010h
;------------------------------
;setup VDP registers (using bios routine WRTVDP)
;------------------------------
ld HL, VDPREG0
ld B, (HL)
ld C, 0
call WRTVdp
ld HL, VDPREG1
ld B, (HL)
ld C, 1
call WRTVdp
ld HL, VDPREG2
ld B, (HL)
ld C, 2
call WRTVdp
ld HL, VDPREG3
ld B, (HL)
ld C, 3
call WRTVdp
ld HL, VDPREG4
ld B, (HL)
ld C, 4
call WRTVdp
ld HL, VDPREG5
ld B, (HL)
ld C, 5
call WRTVdp
ld HL, VDPREG6
ld B, (HL)
ld C, 6
call WRTVdp
ld HL, VDPREG7
ld B, (HL)
ld C, 7
call WRTVdp
;-----------------------------
;clear VRAM
;-----------------------------
ld HL, 0000h
LD BC, 4000h    ;clear all 16k of VRAM
Ld A, 0
call FILVRM
;--------------------------------------------
;Load data from ROM to VRAM by calling LDIRVM function (see below)
;--------------------------------------------
;load PGTs into vram
LD HL, 4800h	;start address of data in ROM (assuming ROM is mapped to 4000h...)
LD BC, 1800h	;size of data
LD DE, 0000h	;start address of data in VRAM
CALL LDIRVM
;load CTs into vram
LD HL, 6000h	;start address of data in ROM (assuming ROM is mapped to 4000h...)
LD BC, 1800h	;size of data
LD DE, 2000h	;start address of data in VRAM
CALL LDIRVM
;load NTs into vram
LD HL, 7800h	;start address of data in ROM (assuming ROM is mapped to 4000h...)
LD BC, 0300h	;size of data
LD DE, 3800h	;start address of data in VRAM
CALL LDIRVM
;-----------------------------------
;endless loop
NOP
JR $-1 
;-----------------------------------
;LDIRVM procedure:
;-----------------------------------
;writes data from RAM/ROM to VRAM
;the parameters are defined as follows:
;HL = start address of data in CPU address space
;BC = size of data (bytes)
;DE = target address in VRAM (first byte)
;-----------------------------------
LDIRVM: equ $005C
;using the bios routine LDIRVM, see MSX specs
;--------------------------------
;WRTVDP - bios routine to write VDP registers
;reg. no. in C, data in B
WRTVdp: equ $0047
;----------------------------------------
;FILVRM - bios routine to fill vram
;vram start address in HL, length of data in BC, data in A
FILVRM: EQU $0056
;------------------------------------
;------------------------------------
;------------------------------------
; Variables in ROM - placed here for easier modification in plugin
;------------------------------------
VDPREG0: EQU $7C00
VDPREG1: EQU $7C01
VDPREG2: EQU $7C02
VDPREG3: EQU $7C03
VDPREG4: EQU $7C04
VDPREG5: EQU $7C05
VDPREG6: EQU $7C06
VDPREG7: EQU $7C07
;-------------------
seek 3C00h
db 00000010b, 11100010b, 00001110b, 11111111b, 00000011b, 01110110b, 00000011b, 07h


