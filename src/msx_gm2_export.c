/**
 * msx_gm2_export - MSX graphics mode II exporter plug-in for GIMP
 * Copyright (C) 2019 Thies Hecker
 *
 * This file is part of the gimp-msx-plugins.
 *
 * gimp-msx-plugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * This code is based on the GIMP Plug-in Template
 * - see its copyright and license notice below:
 *
 *   GIMP Plug-in Template
 *   Copyright (C) 2000-2004  Michael Natterer <mitch@gimp.org> (the "Author").
 *   All Rights Reserved.
 *
 *   Permission is hereby granted, free of charge, to any person obtaining a
 *   copy of this software and associated documentation files (the "Software"),
 *   to deal in the Software without restriction, including without limitation
 *   the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *   and/or sell copies of the Software, and to permit persons to whom the
 *   Software is furnished to do so, subject to the following conditions:
 *
 *   The above copyright notice and this permission notice shall be included
 *   in all copies or substantial portions of the Software.
 *
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *   OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 *   THE AUTHOR BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 *   IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 *   CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 *   Except as contained in this notice, the name of the Author of the
 *   Software shall not be used in advertising or otherwise to promote the
 *   sale, use or other dealings in this Software without prior written
 *   authorization from the Author.
*/
#include <stdio.h>
#include <math.h>
#include <string.h>
#include <libgimp/gimp.h>
#include <libgimp/gimpui.h>
#include <libgimpwidgets/gimpwidgets.h>
#include <libgimp/gimpenums.h>

#include "../config.h"
#include "utility.h"
#include "types.h"

static void query (void);
static void run   (const gchar      *name,
                   gint              nparams,
                   const GimpParam  *param,
                   gint             *nreturn_vals,
                   GimpParam       **return_vals);

static gint* get_block_coordinates(gint block_no, gint region);
static guint* find_unique_blocks(GimpDrawable *drawable, guchar region);
static void create_rom_image(gchar *filename, guchar bg_color);
static gboolean pre_check(GimpDrawable *drawable);
static void check (GimpDrawable *drawable);
static void convert  (GimpDrawable *drawable);
static void export_files(gchar *filename);
static gboolean gm2exp_dialog(GimpDrawable *drawable);
void read_combo_box (GtkWidget *widget, gpointer data);
void read_spin_button (GtkWidget *widget, gpointer data);

GimpPlugInInfo PLUG_IN_INFO =
{
  NULL,
  NULL,
  query,
  run
};

static ExportValues ex_vals =
{
  .transp_setting = USE_ALPHA,  //use transparency
  .add_bload = FALSE,	//add BLOAD header
  .check_only = FALSE,	// check only
  .compress = TRUE,	// identify unique blocks and save image accordingly
  .split_files = TRUE,	// creates seperate files for each screen region
  .export_asm = TRUE,	// export an assembly file, which can run to show the image on MSX
  .dither_mode = 2,	// dither mode - do not use enum (as names have changes between 2.8 and 2.10!)
  .indexer = Internal,	// color indexer
  .bg_color = 4	// background color for ROM image, 4 = blue
};

static ImageData im_data;	// struct to store the image data

MAIN()

static void
query (void)
{
  static GimpParamDef args[] =
  {
    {
      GIMP_PDB_INT32,
      "run-mode",
      "Run mode"
    },
    {
      GIMP_PDB_IMAGE,
      "image",
      "Input image"
    },
    {
      GIMP_PDB_DRAWABLE,
      "drawable",
      "Input drawable"
    }
  };

  gimp_install_procedure (
    "plug-in-msxex3",
    PACKAGE_STRING,
    "Export Name, Pattern and Color Table to MSX Graphics Mode II binary format",
    "Thies Hecker",
    "Copyright Thies Hecker",
    "2019",
    "_MSX GMII Exporter",
    "RGB*, GRAY*, INDEXED*",
    GIMP_PLUGIN,
    G_N_ELEMENTS (args), 0,
    args, NULL);

  gimp_plugin_menu_register ("plug-in-msxex3",
                             "<Image>/Filters/Misc");
}

static void
run (const gchar      *name,
     gint              nparams,
     const GimpParam  *param,
     gint             *nreturn_vals,
     GimpParam       **return_vals)
{
  static GimpParam  values[1];
  GimpPDBStatusType status = GIMP_PDB_SUCCESS;
  GimpRunMode       run_mode;
  GimpDrawable     *drawable;
  gboolean is_indexed, combinations_valid;
  gint32 drawable_id;
  gint32 image_id;
  // guchar reg;

  /* Setting mandatory output values */
  *nreturn_vals = 1;
  *return_vals  = values;

  values[0].type = GIMP_PDB_STATUS;
  values[0].data.d_status = status;

  /* Getting run_mode - we won't display a dialog if
   * we are in NONINTERACTIVE mode
   */
  run_mode = param[0].data.d_int32;
  drawable_id = param[2].data.d_drawable;
  //image_id = param[1].data.d_image;
  image_id = gimp_drawable_get_image(drawable_id);

  /*  Get the specified drawable  */
  drawable = gimp_drawable_get (drawable_id);

  switch (run_mode)
	  {
	  case GIMP_RUN_INTERACTIVE:
		// Get options last values if needed
		gimp_get_data ("plug-in-msxex3", &ex_vals);

		// Display the dialog
		if (! gm2exp_dialog (drawable))
		  return;
		break;

	  case GIMP_RUN_NONINTERACTIVE:
		if (nparams != 4)
		  status = GIMP_PDB_CALLING_ERROR;
		if (status == GIMP_PDB_SUCCESS)
		printf("non interactive mode...");
		break;

	  case GIMP_RUN_WITH_LAST_VALS:
		//printf("run with last vals...");
		gimp_get_data ("plug-in-msxex3", &ex_vals);
		break;

	  default:
		break;
	  }

  //check if the options contain forbidden combinations
  combinations_valid = check_option_combinations(ex_vals);
  if (!combinations_valid){
	  return;
  }

  //call resize function
  resize_image(image_id);

  // if the image is not indexed it has to be indexed to the MSX palette
  //TODO: Should check, if an image is indexed unintended
  is_indexed = gimp_drawable_is_indexed(drawable->drawable_id);
  if (! is_indexed){
	  drawable = index_image(image_id, ex_vals.transp_setting, ex_vals.dither_mode, ex_vals.indexer);
	  if (drawable == NULL){
		  return;
	  }
  }
  else{
	printf("Image already indexed...nothing to do.\n");
  }

  // if check_only is TRUE the gm2_check function will be used instead of gm2_export
  if (ex_vals.check_only){
	  printf("Checking for compatibility only...\n");
	  check(drawable);
  }
  else{
	  convert (drawable);
  }

  gimp_displays_flush ();
  gimp_drawable_detach (drawable);

  if (run_mode == GIMP_RUN_INTERACTIVE)
    gimp_set_data ("plug-in-msxex3", &ex_vals, sizeof (ExportValues));
  	printf("GIMP run interactive...\n");

  /* revert back to RGB image - otherwise consecutive runs with transparency
   * on/off will mess up the identified colors
   */
  gimp_image_convert_rgb(image_id);
  return;
}

static guint*
find_unique_blocks(GimpDrawable* drawable, guchar region){
	/// Finds unique blocks in the image for specific screen region and calculate name table
	/**
	 * @param: drawable - gimp drawable object
	 * @param: region 0 = top, 1 = mid, 2 = bottom
	 *
	 * @returns array[256] (optimized name table)
	 */
	//TODO: Maybe the returned nt_list should also be saved in im_data struct...
	guint i, j, k, unique_count;
	gint *coordinates, xpos, ypos;
	static guint nt_list[256]; //_t, *nt_list_m, *nt_list_b;	//name table lists

	GimpPixelRgn whole_image;
	guchar *buf, *current_buf;
	gboolean blockfound;
	guint buf_size;

	//buffers size: 8x8 pixel x 1 for indexed and x 2 indexed + alpha
	if (gimp_drawable_has_alpha(drawable->drawable_id)){
		buf_size = 128;
	}
	else{
		buf_size = 64;
	}

	buf = g_new(guchar, buf_size);
	current_buf = g_new(guchar, buf_size);

	/*
	if (gimp_drawable_has_alpha(drawable->drawable_id)){
		buf = g_new(guchar, 128);
		current_buf = g_new(guchar, 128);
	}
	else{
		buf = g_new(guchar, 64);
		current_buf = g_new(guchar, 64);
	}
	*/

	//gimp_drawable_free_shadow (drawable->drawable_id);
	gimp_pixel_rgn_init(&whole_image, drawable, 0, 0, 256, 192, FALSE, FALSE);

	unique_count = 0;

	printf("Searching for unique blocks in region [%d/2]...\n", region);

	for (i = 0; i < 256; i++){
		coordinates = get_block_coordinates(i, region);
		xpos = *coordinates;
		ypos = *(coordinates + 1);

		gimp_pixel_rgn_get_rect(&whole_image, current_buf, xpos, ypos, 8, 8);

		//blockfound = FALSE;

		// loop through all blocks before this block to find a match
		for (j = 0; j <= i; j++){
			coordinates = get_block_coordinates(j, region);
			xpos = *coordinates;
			ypos = *(coordinates + 1);
			gimp_pixel_rgn_get_rect(&whole_image, buf, xpos, ypos, 8, 8);

			//check if all 64 color values of the blocks are identical
			blockfound = TRUE;
			for (k = 0; k < buf_size; k++){
				if (*(buf + k) != *(current_buf + k)){
					blockfound = FALSE;
					break;
				}
			}
			if (blockfound){
				//blockfound = TRUE;
				nt_list[i] = j;
				if (j == i){	// only found a match in itself...
					printf("Region [%d/2], Block [%d/255]: Unique block\n", region, i);
					unique_count++;
				}
				else{
					printf("Region [%d/2], Block [%d/255]: Uses block no. %d\n", region, i, j);
					break;
				}
			}
		}
	}
	printf("Found %d unique blocks in region %d.\n", unique_count, region);
	return nt_list;
}

static gint* get_block_coordinates(gint block_no, gint region){
	/// Returns the x,y coordinates belonging to specific block in a specific region
	/**
	 * Valid block number are 0...255, valid regions are 0 (TOP), 1 (MID), 2 (BOTTOM)
	 */

	gint x, y, row;
	static gint coord[2];

	// printf("Caluclating block coordinates for block %d in region %d...\n", block_no, region);

	row = (gint) (block_no / 32);

	// printf("Block row is %d, ", row);

	y = row * 8 + 64 * region;
	x = (block_no - 32 * row) * 8;

	// printf("x = %d, y = %d\n", x, y);

	coord[0] = x;
	coord[1] = y;

	return coord;

}


static gboolean pre_check(GimpDrawable *drawable){
	/// checks if the current layer/image is suitable for conversion
	/**
	 * checks if alpha channel is used, resolution is correct...
	 *
	 * @param: drawable - gimp drawable object
	 *
	 * @returns TRUE if everything is okay
	 */

	gint width, height;
	gboolean valid = TRUE;
	gboolean has_alpha;

	/*get width and height of drawable*/
	width = gimp_drawable_width (drawable->drawable_id);
	height = gimp_drawable_height (drawable->drawable_id);

	/*check if drawable dimensions are okay (8x8 px blocks)
	if (((float)width/8-width/8 != 0) || ((float)height/8-height/8 != 0)){
		g_message("Resolution must be multiples of 8 for width and height.\n");
		valid = FALSE;
	}
	*/

	/*check if drawable dimensions are okay (8x8 px blocks)*/
	if ((width != 256) || (height != 192)){
		g_message("Resolution must be 256x192 px (width x height).\n");
		valid = FALSE;
	}

	/*check if drawable has alpha*/
	has_alpha = gimp_drawable_has_alpha (drawable->drawable_id);
	if (has_alpha == TRUE){
		g_message("Please remove alpha channel of drawable.\n The plugin can only handle indexed layers without alpha channel.");
		valid = FALSE;
	}

	return valid;
}


static void
check (GimpDrawable *drawable)
{
	/// checks if the image is compatible with graphics mode II
	/**
	 * Creates an output of how many 1x8 pixel rows violate GM II
	 * Creates a new layer showing the invalid rows in red color.
	 *
	 * @param: drawable - gimp drawable object
	 */
	// const gchar *layername = "Layer1";
	gint width, height; //channels; //, x1, wx, y1, hy, x2, y2;
	gint i,j,k,l, colcount, errorcount;
	//GimpImageType type1;
	//gdouble opacity;
	//GimpLayerModeEffects mode;
	gint32 layer1, cur_image_ID;
	GimpDrawable *layer1p;
	//gboolean layerins; //has_alpha;
	GimpPixelRgn rgn_in, rgn_out;
	guchar *rowin, *rowout, seccol;

	rowin = g_new (guchar, 1*8);
	rowout = g_new (guchar, 1*8);

	//channels = gimp_drawable_bpp (drawable->drawable_id);

	cur_image_ID = gimp_drawable_get_image (drawable->drawable_id);

	gimp_drawable_free_shadow (drawable->drawable_id);

	/*get width and height of drawable*/
	width = gimp_drawable_width (drawable->drawable_id);
	height = gimp_drawable_height (drawable->drawable_id);

	// check if drawable is suitable
	if (!pre_check(drawable)){
		return;
	}

/*	create a region on the source image (active drawable)	*/
	gimp_pixel_rgn_init (&rgn_in, drawable, 0, 0, width, height, FALSE, FALSE);

/*	create a layer and a GimpDrawable pointer to the layer for results output	*/
	layer1 = gimp_layer_new_from_drawable (drawable->drawable_id, cur_image_ID);
	/*layer1 = gimp_layer_new(cur_image_ID, layername, wx, hy, type1, opacity, mode);*/
	gimp_image_insert_layer(cur_image_ID, layer1, 0, -1);
	layer1p = gimp_drawable_get (layer1);

/*	define the output to be on the newly created layer	*/
	gimp_pixel_rgn_init (&rgn_out, layer1p, 0, 0, width, height, TRUE, FALSE);

	errorcount = 0;

	for (i = 0; i < (width/8); i++){
		for (j = 0; j < height; j++){
			gimp_pixel_rgn_get_row (&rgn_in, rowin, i*8, j, 8);
			/*  count colors in row */
			seccol = rowin[0];
			colcount = 1;
			for (k = 1; k < 8; k++){
				/*check if the color of pixel k is neither the color of the first pixel nor the second color*/
				if ((rowin[k] != rowin[0]) && (rowin[k] != seccol)){
					seccol = rowin[k];
					colcount++;
				}
			}
			/* if more than 2 colors per row, make row red (indexed color 5)*/
			if (colcount > 2){
				for (l = 0; l < 8; l++){
					rowout[l]=6;
				}
				gimp_pixel_rgn_set_row (&rgn_out, rowout, i*8, j, 8);
				errorcount++;
			}

		}
	}

	g_message("Image contains %d violations of graphics mode II.\n", errorcount);


/*	free mem */
  	g_free (rowin);
  	g_free (rowout);

/*	gimp_drawable_merge_shadow (drawable->drawable_id, TRUE);	*/
	gimp_drawable_flush (drawable);
  	gimp_drawable_update (drawable->drawable_id, 0, 0, width, height);
}

static void
convert (GimpDrawable *drawable)
{
	/// Converts the image data into graphics mode II name, pattern and color tables
	/**
	 * @param: drawable - gimp drawable object
	 * options are taken from the ex_vals struct
	 *
	 * Image data will be stored in the im_data struct,
	 * the converted image is added as a new layer
	 */
	// const gchar *layername = "Layer1";

	const gchar *extROM = ".rom";
	gint width, height; //channels; //, x1, wx, y1, hy, x2, y2;
	gint i,  k, l, m, n, region, no_colors; //j,colcount,errorcount,
	gint bytecount, xpos, ypos;  //blockno,blocksx, blocksy,
	guint8 CTbyte, PTbyte, NTbyte; // blockcount,NTnumber;
	// GimpImageType type1;
	// gdouble opacity;
	// GimpLayerModeEffects mode;
	gint32 layer1, cur_image_ID;
	GimpDrawable *layer1p;
	// gboolean layerins //has_alpha;
	GimpPixelRgn rgn_in, rgn_out;
	guchar *rowin, *rowout, seccol, color1, color2, row_buf_size;
	gchar *filename, *dirname, *shortfilename, *romfilename;
	gint  block, nt_entry; //fnamelength,
	gint *coordinates;
	guint cur_nt[256];
	guint *nt;

	guchar nt_entry_map[768];
	guchar msx_colors_row[8];

	gdouble pow(gdouble x, gdouble y); 

	if (gimp_drawable_has_alpha(drawable->drawable_id)){
		row_buf_size = 16;	// 8 pixels * 2 (alpha, index)
	}
	else{
		row_buf_size = 8;	// 8 pixels * 1 (index)
	}

	rowin = g_new (guchar, row_buf_size);
	rowout = g_new (guchar,row_buf_size);
	filename = g_new (gchar, 256);
	shortfilename = g_new (gchar, 256);
	dirname = g_new (gchar, 256);
	romfilename = g_new (gchar, 256);

	//channels = gimp_drawable_bpp (drawable->drawable_id);
	cur_image_ID = gimp_drawable_get_image (drawable->drawable_id);

	/*get file name of current image and create files for NT, PT and CT in the same folder, having the same file name but extension .nt, .pt and .ct respectively*/
	filename = gimp_image_get_filename (cur_image_ID);

	if (filename == NULL){
		g_message("You need to save the file before you run the plug-in!\n");
		return;
	}

	shortfilename = gimp_image_get_name(cur_image_ID);
	strncpy(dirname, filename, strlen(filename) - strlen(shortfilename));

	/*remove "file://" URI - fopen does not seem to handle this properly*/
	//memmove(filename, filename+6, strlen(filename));
	//fnamelength = strlen(filename);

	gimp_drawable_free_shadow (drawable->drawable_id);

	/*get width and height of drawable*/
	width = gimp_drawable_width (drawable->drawable_id);
	height = gimp_drawable_height (drawable->drawable_id);

	/*
	// check if drawable is suitable
	if (!pre_check(drawable)){
		return;
	}
	*/

	//create a region on the source image (active drawable)
	gimp_pixel_rgn_init (&rgn_in, drawable, 0, 0, width, height, FALSE, FALSE);

	PTbyte = 0;
	CTbyte = 0;
	bytecount = 0;
	// blockcount = 0;

	/*
	if (ex_vals.transp_setting == NO_TRANSPARENCY){
		no_colors = 15;
	}
	else{
		no_colors = 16;
	}
	*/

	no_colors = 16;
	guchar colors[no_colors];	//array to store location of each

    /*	create a layer and a GimpDrawable pointer to the layer for results output	*/
	layer1 = gimp_layer_new_from_drawable (drawable->drawable_id, cur_image_ID);
	/*layer1 = gimp_layer_new(cur_image_ID, layername, wx, hy, type1, opacity, mode);*/
	gimp_image_insert_layer(cur_image_ID, layer1, 0, -1);
	layer1p = gimp_drawable_get (layer1);

    /*	define the output to be on the newly created layer	*/
	gimp_pixel_rgn_init (&rgn_out, layer1p, 0, 0, width, height, TRUE, FALSE);

	gimp_progress_init("Calculating PGT and CT values...");

	for (region = 0; region < 3; region++){

		// assign name table values & eventually identify unique blocks
		if (ex_vals.compress){
			nt = find_unique_blocks(drawable, region);
			for (i = 0; i < 256; i++){
				cur_nt[i] = *(nt + i);
			}
		}
		else{
			for (i = 0; i < 256; i++){
				cur_nt[i] = i;
			}
		}

		printf("Calculating PGT and CT bytes for region %d...\n", region);
		nt_entry = 0;
		
		for (block=0; block < 256; block++){
			gimp_progress_update((double)(region * 256 + block) / (double)768);
			// printf("Assigning block %d (uses block %d data)...\n", block, cur_nt[block]);

			if (cur_nt[block] == block){	// this block is a unique/master block

				// fill nt_entry_map (this is the actual name table...)
				printf("Region[%d/2], Block[%d/255]: NT entry %d, calculating PGT and CT...\n", region, block, nt_entry);
				//printf("Region[%d/2], Block[%d/255]: Unique block, NT entry %d\r", region, block, nt_entry);
				nt_entry_map[block + region * 256] = nt_entry;


				coordinates = get_block_coordinates(block, region);
				xpos = *coordinates;
				ypos = *(coordinates + 1);

				//printf("Coordinates for block %d: x=%d, y=%d\n", block, xpos, ypos);

				/*get PT and CT data*/
				for (k = 0; k < 8; k++){

					//reset pattern and color byte
					PTbyte = 0;
					CTbyte = 0;
					//fill colors array with zeros
					for (m = 0; m < no_colors; m++){
						colors[m] = 0;
					}
					//printf("color array filled with zeros\n");
					gimp_pixel_rgn_get_row (&rgn_in, rowin, xpos, ypos+k, 8);

					//count occurence of colors
					printf("Colors in row %d: ", k);
					for (l = 0; l < 8; l++ ){
						if (gimp_drawable_has_alpha(drawable->drawable_id)){

							if (rowin[2*l+1] == 0){	// alpha = 0
								colors[0]++;
								msx_colors_row[l] = 0;
							}
							else{
								if (ex_vals.transp_setting == USE_COLOR_FF00EB){
									colors[rowin[2*l]]++;	// 15 color palette
									msx_colors_row[l] = rowin[2*l];
								}
								else{
									colors[rowin[2*l]+1]++;
									msx_colors_row[l] = rowin[2*l]+1;
								}
							}
						}
						else{
							if (ex_vals.transp_setting == USE_COLOR_FF00EB){
								colors[rowin[l]]++;	// 16 color palette
								msx_colors_row[l] = rowin[l];
							}
							else{
								colors[rowin[l]+1]++;
								msx_colors_row[l] = rowin[l]+1; //15 color palette
							}
						}
						printf("%d, ", msx_colors_row[l]);
					}
					printf("\n");

					//printf("color array filled with color counts\n");
					//find color 1 and 2 (most and second most used colors)
					if (colors[0] >= colors[1]){
						color1 = 0;
						color2 = 1;
					} else {
						color1 = 1;
						color2 = 0;
					}

					for (m = 2; m < no_colors; m++){
						if (colors[m] > colors[color1]){
							color2 = color1;
							color1 = m;
						}
						else if (colors[m] > colors[color2]){
							color2 = m;
						}
					}

					/*
					printf("Colors for row %d:\n", k);
					for (m = 0; m < no_colors; m++){
						printf("%d", colors[m]);
					}
					printf("\n");
					*/

					//fill output row with color 2
					for (n = 0; n < 8; n++){
						if (gimp_drawable_has_alpha(drawable->drawable_id)){
							// USE_COLOR... is the only option with the 16 color palette
							if (ex_vals.transp_setting == USE_COLOR_FF00EB){
								rowout[2*n] = color2;
							}
							else{
								rowout[2*n] = color2-1;	//shift color index to 15 color palette
							}

							if (color2 == 0){
								rowout[2*n+1] = 0;
							}
							else{
								rowout[2*n+1] = 255;	//no transparency
							}
						}
						else{
							// USE_COLOR... is the only option with the 16 color palette
							if (ex_vals.transp_setting == USE_COLOR_FF00EB){
								rowout[n] = color2;
							}
							else{
								rowout[n] = color2-1;	//shift color index to 15 color palette
							}
						}

					}
					//printf("output row filled with color 2\n");
					//calculate value of pattern byte (at 2**(7-l) to byte if color 1 other wise add zero / do nothing)
					for (l = 0; l < 8; l++){
						if (msx_colors_row[l] == color1){
							PTbyte += pow(2, 7-l);

							if (gimp_drawable_has_alpha(drawable->drawable_id)){
								if (ex_vals.transp_setting == USE_COLOR_FF00EB){
									rowout[2*l] = color1;
								}
								else{
									rowout[2*l] = color1-1;	//shift color index to 15 color palette
								}

								if (color1 == 0){
									rowout[2*l+1] = 0;
								}
								else{
									rowout[2*l+1] = 255;	//no transparency
								}
							}
							else{
								// USE_COLOR... is the only option with the 16 color palette
								if (ex_vals.transp_setting == USE_COLOR_FF00EB){
									rowout[l] = color1;
								}
								else{
									rowout[l] = color1-1;	//shift color index to 15 color palette
								}
							}
						}
						//assign further colors to either color 1 or 2
						else if (msx_colors_row[l] != color2){
							if ((l>0 && msx_colors_row[l-1] == color1)
									|| (l<7 && msx_colors_row[l+1] == color1)) {
								PTbyte += pow(2, 7-l);

								if (gimp_drawable_has_alpha(drawable->drawable_id)){
									if (ex_vals.transp_setting == USE_COLOR_FF00EB){
										rowout[2*l] = color1;
									}
									else{
										rowout[2*l] = color1-1;	//shift color index to 15 color palette
									}

									if (color1 == 0){
										rowout[2*l+1] = 0;
									}
									else{
										rowout[2*l+1] = 255;	//no transparency
									}
								}
								else{
									// USE_COLOR... is the only option with the 16 color palette
									if (ex_vals.transp_setting == USE_COLOR_FF00EB){
										rowout[l] = color1;
									}
									else{
										rowout[l] = color1-1;	//shift color index to 15 color palette
									}
								}
							}
						}
					}
					// printf("Row %d: Color1 = %d, color2 = %d, PTbyte = %d\n", k, color1, color2, PTbyte);

					bytecount++;

					CTbyte = color1*16+color2;
					/*define byte for color table*/
					/*
					if (ex_vals.transp_setting == USE_COLOR_FF00EB
							|| ex_vals.transp_setting == USE_ALPHA){
						CTbyte = color1*16+color2;
					}
					else{
						CTbyte = (color1+1)*16+(color2+1);	// since the palette is lacking the first MSX color (i.e. transp.)
					}
					*/

					//write output image to new layer
					gimp_pixel_rgn_set_row (&rgn_out, rowout, xpos, ypos+k, 8);
					//printf("information written to results layer");

					// write CT & PT to image data container
					im_data.ct_data[region][nt_entry * 8 + k] = CTbyte;
					im_data.pt_data[region][nt_entry * 8 + k] = PTbyte;
				}

				nt_entry++;

			}

			else{
				nt_entry_map[block + region * 256] = nt_entry_map[cur_nt[block] + region * 256];
				printf("Region[%d/2], Block[%d/255]: re-using NT entry %d.\n", region, block, nt_entry-1);
			}
			// write data to name table
			NTbyte = nt_entry_map[block + region * 256];

			// assign image data for this block
			im_data.nt_data[region][block] = NTbyte;
			im_data.unique_blocks[region] = nt_entry;
		}
	}

	gimp_progress_end();

	// export binary data of tables to files
	export_files(filename);

	// export ROM image
	if (ex_vals.export_asm){
		strcpy(romfilename, filename);
		strcat(romfilename, extROM);
		printf("exporting ROM to %s...\n", romfilename);
		create_rom_image(romfilename, (guchar)ex_vals.bg_color);
		//create_rom_image();
	}

	/*print some output about the files created*/
	g_message("%d bytes of data written to PGT and CT file.\n", bytecount);
	g_message("Files exported to:\n%s\n", dirname);

	/*free mem */
  	g_free (rowin);
  	g_free (rowout);
  	g_free (filename);
	g_free (romfilename);
    gimp_drawable_flush (drawable);
}


static void export_files(gchar *filename){
	/// Exports the files for the name, pattern and color tables
	/**
	 * @param: filename - should be the filename of the image
	 *
	 * The individual files will be extended by .pt, .nt, .ct
	 *
	 * or
	 *
	 * .pt1, pt2, .pt3, nt1,...(if split_files option in ex_vals is TRUE)
	 */

	gchar *ptfilename, *ctfilename, *ntfilename;
	guchar *header, *headerNT;
	guint16 header_size;
	FILE *PTfile, *CTfile, *NTfile;
	const gchar *fmode = "wb";
	const gchar delim[] = ".";

	const gchar *extPT = ".pt";
	const gchar *extCT = ".ct";
	const gchar *extNT = ".nt";

	ptfilename = g_new (gchar, 256);
	ctfilename = g_new (gchar, 256);
	ntfilename = g_new (gchar, 256);

	gint fnamelength, i, region;

	// if export should be split in screen regions
	gchar ptfilenames[3][256];
	gchar ctfilenames[3][256];
	gchar ntfilenames[3][256];
	FILE *PTfiles[3];
	FILE *CTfiles[3];
	FILE *NTfiles[3];

	gchar reg_str[3][2];
	strcpy(reg_str[0], "1");
	strcpy(reg_str[1], "2");
	strcpy(reg_str[2], "3");

	/*remove "file://" URI - fopen does not seem to handle this properly*/
	//memmove(filename, filename+6, strlen(filename));
	fnamelength = strlen(filename);

	gchar *name = strtok(filename, delim);

	/*
	strncpy(ptfilename, filename, fnamelength-3);
	strncpy(ctfilename, filename, fnamelength-3);
	strncpy(ntfilename, filename, fnamelength-3);
	*/

	strcpy(ptfilename, name);
	strcpy(ctfilename, name);
	strcpy(ntfilename, name);

	strcat(ptfilename, extPT);
	strcat(ctfilename, extCT);
	strcat(ntfilename, extNT);

	// if split option selected PT, CT and NT will be created for each screen region
	if (ex_vals.split_files){
		for (i = 0; i < 3; i++){

			strcpy(ptfilenames[i], ptfilename);
			strcat(ptfilenames[i], reg_str[i]);
			printf("pt file name: %s\n", ptfilenames[i]);
			PTfiles[i] = fopen(ptfilenames[i], fmode);

			strcpy(ctfilenames[i], ctfilename);
			strcat(ctfilenames[i], reg_str[i]);
			CTfiles[i] = fopen(ctfilenames[i], fmode);

			strcpy(ntfilenames[i], ntfilename);
			strcat(ntfilenames[i], reg_str[i]);
			NTfiles[i] = fopen(ntfilenames[i], fmode);
		}
	}
	else{
		PTfile = fopen(ptfilename, fmode);
		CTfile = fopen(ctfilename, fmode);
		NTfile = fopen(ntfilename, fmode);
	}

	for (region = 0; region < 3; region++){

		if (ex_vals.split_files){
			PTfile = *(PTfiles + region);
			CTfile = *(CTfiles + region);
			NTfile = *(NTfiles + region);
		}

		// add BLOAD headers
		if (ex_vals.add_bload){
			printf("Adding BLOAD headers...\n");
			if (ex_vals.split_files){
				header_size = (guint16) (im_data.unique_blocks[region] * 8);	//size of PGT and CT in bytes
				header = create_bload_header(header_size, 0);	// start address = 0...

				fwrite(header, sizeof(header)-1, 1, CTfile);
				fwrite(header, sizeof(header)-1, 1, PTfile);

				headerNT = create_bload_header((guint16)256, 0);	// NT size should always be 256
				fwrite(headerNT, sizeof(headerNT)-1, 1, NTfile);
			}
			// if the split_files option is disabled only do this once (only for the first region)
			else if (!ex_vals.split_files && region == 0){
				header = create_bload_header((guint16)6144, 0);	// PGT/CT size should be 3x2048

				fwrite(header, sizeof(header)-1, 1, CTfile);
				fwrite(header, sizeof(header)-1, 1, PTfile);

				headerNT = create_bload_header((guint16)768, 0);	// NT size should be 3x256
				fwrite(headerNT, sizeof(headerNT)-1, 1, NTfile);
			}
		}

		// write name table
		fwrite(&im_data.nt_data[region], sizeof(im_data.nt_data[region]), 1, NTfile);

		// write pattern and color table
		if (ex_vals.compress){
			fwrite(&im_data.pt_data[region], im_data.unique_blocks[region]*8, 1, PTfile);
			fwrite(&im_data.ct_data[region], im_data.unique_blocks[region]*8, 1, CTfile);

		}
		else{
			fwrite(&im_data.pt_data[region], sizeof(im_data.pt_data[region]), 1, PTfile);
			fwrite(&im_data.ct_data[region], sizeof(im_data.ct_data[region]), 1, CTfile);
		}

		// if separate files for each region - close now, otherwise after loop has finished...
		if (ex_vals.split_files){
			fflush(PTfile);
			fclose (PTfile);
			fflush(CTfile);
			fclose (CTfile);
			fflush(NTfile);
			fclose (NTfile);
		}
	}

	/*close files*/
	if (!ex_vals.split_files){
		fflush(PTfile);
		fclose (PTfile);
		fflush(CTfile);
		fclose (CTfile);
		fflush(NTfile);
		fclose (NTfile);
	}

	g_free (ptfilename);
	g_free (ctfilename);
	g_free (ntfilename);
}

static void
create_rom_image(gchar *filename, guchar bg_color){
	/// Creates a 16k rom image for the MSX
	/**
	 * Creates a ROM file for testing the image export on an MSX
	 *
	 * @param: filename - filename of the ROM image
	 * @param: bg_color - MSX back drop color (to highlight transparent pixels)
	 */

	FILE *rom_file;
	rom_file = fopen(filename, "wb");

	guint i;
	guint8 byte;

	guint pt_start = 0x0800;	// start addr. of pattern table data in ROM image
	guint ct_start = 0x2000;	// start addr. of color table data in ROM image
	guint nt_start = 0x3800;	// start addr. of name table data in ROM image
	guint vdp_start = 0x3C00;	// start addr. of VDP register values
	guint bg_color_addr = 0x3C07;	// address of back drop color value

	//VDP register default values
	const gchar vdp_defaults[] = {0x02, 0xE2, 0x0E, 0xFF, 0x03, 0x76, 0x03, 0x07};

	//header with the opcode to setup the MSX's VDP, load data in VRAM,...
	// the source for this code can be found in src/test.asm
	const gchar header[] = {0x41, 0x42, 0x10, 0x40, 0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x21, 0x00, 0x7C,
			0x46, 0x0E, 0x00, 0xCD, 0x47, 0x00, 0x21, 0x01, 0x7C, 0x46, 0x0E,
			0x01, 0xCD, 0x47, 0x00, 0x21, 0x02, 0x7C, 0x46, 0x0E, 0x02, 0xCD,
			0x47, 0x00, 0x21, 0x03, 0x7C, 0x46, 0x0E, 0x03, 0xCD, 0x47, 0x00,
			0x21, 0x04, 0x7C, 0x46, 0x0E, 0x04, 0xCD, 0x47, 0x00, 0x21, 0x05,
			0x7C, 0x46, 0x0E, 0x05, 0xCD, 0x47, 0x00, 0x21, 0x06, 0x7C, 0x46,
			0x0E, 0x06, 0xCD, 0x47, 0x00, 0x21, 0x07, 0x7C, 0x46, 0x0E, 0x07,
			0xCD, 0x47, 0x00, 0x21, 0x00, 0x00, 0x01, 0x00, 0x40, 0x3E, 0x00,
			0xCD, 0x56, 0x00, 0x21, 0x00, 0x48, 0x01, 0x00, 0x18, 0x11, 0x00,
			0x00, 0xCD, 0x5C, 0x00, 0x21, 0x00, 0x60, 0x01, 0x00, 0x18, 0x11,
			0x00, 0x20, 0xCD, 0x5C, 0x00, 0x21, 0x00, 0x78, 0x01, 0x00, 0x03,
			0x11, 0x00, 0x38, 0xCD, 0x5C, 0x00, 0x00, 0x18, 0xFD};

	//fill rom with 16k of zeros
	byte = 0;
	for (i = 0; i < 16384; i++){
		fwrite(&byte, sizeof(byte), 1, rom_file);
	}

	//add header
	fseek(rom_file, 0, SEEK_SET);
	//printf("fseek to 0 - size of header: %d\n", sizeof(header));
	fwrite(&header, sizeof(header), 1, rom_file);

	//write name table
	fseek(rom_file, nt_start, SEEK_SET);
	fwrite(&im_data.nt_data, sizeof(im_data.nt_data), 1, rom_file);
	//write pattern table
	fseek(rom_file, pt_start, SEEK_SET);
	fwrite(&im_data.pt_data, sizeof(im_data.pt_data), 1, rom_file);
	//write color table
	fseek(rom_file, ct_start, SEEK_SET);
	fwrite(&im_data.ct_data, sizeof(im_data.ct_data), 1, rom_file);

	//write VDP setup values
	fseek(rom_file, vdp_start, SEEK_SET);
	fwrite(&vdp_defaults, sizeof(vdp_defaults), 1, rom_file);
	//setup back drop color
	fseek(rom_file, bg_color_addr, SEEK_SET);
	fwrite(&bg_color, sizeof(bg_color), 1, rom_file);

	fclose(rom_file);

}

void read_combo_box (GtkWidget *widget, gpointer data){
	/// Assigns the selected item ID of a combo box to pointer data
	/**
	 * @param: widget must be a GtkComboBox
	 * @param: data is a pointer to an gint object
	 */
	gint *selected_item = (gint*) data;
	//gint selected_item;

	*selected_item = gtk_combo_box_get_active(GTK_COMBO_BOX (widget));
	//ex_vals.dither_mode = selected_item;

	printf("Combo box update - selected item = %d\n", *selected_item);
}

void read_spin_button (GtkWidget *widget, gpointer data){
	/// Assigns the value of a spin button to pointer data
	/**
	 * @param: widget must be a Spin button
	 * @param: data is a pointer to an gint object
	 */
	gint *value = (gint*) data;
	//gint selected_item;

	*value = gtk_spin_button_get_value_as_int(widget);
	//ex_vals.dither_mode = selected_item;

	printf("Spin button update - value = %d\n", *value);
}

static gboolean
gm2exp_dialog (GimpDrawable *drawable)
{
	/// Creates the GUI for the plug-in
	GtkWidget *dialog;
	GtkWidget *main_vbox;

	GtkWidget *exp_opt_vbox;
	GtkWidget *color_opt_vbox;

	GtkWidget *hbox_indexer;
	GtkWidget *hbox_dither;
	GtkWidget *hbox_transparency;
	GtkWidget *hbox_bg_color;

	GtkWidget *frame_export;
	GtkWidget *frame_colors;

	//GtkWidget *checkbox_transparency;
	GtkWidget *checkbox_add_bload;
	GtkWidget *checkbox_check_only;
	GtkWidget *checkbox_compress;
	GtkWidget *checkbox_split;
	GtkWidget *checkbox_export_asm;

	GtkWidget *combobox_dither;
	GtkWidget *combobox_indexer;
	GtkWidget *combobox_transparency;
	// GtkWidget *enum_combo_dither;

	GtkWidget *spinner_bg_color;

	GtkWidget *label_dither;
	GtkWidget *label_indexer;
	GtkWidget *label_transparency;
	GtkWidget *label_bg_color;

	GtkWidget *frame_label_exp;
	GtkWidget *frame_label_color;

	GtkWidget *alignment_exp;
	GtkWidget *alignment_color;

	gboolean   run;

	gimp_ui_init ("gm2exp", FALSE);

	dialog = gimp_dialog_new ("MSX Graphics mode II exporter", "gm2exp",
						  NULL, 0,
						  gimp_standard_help_func, "plug-in-msxex3",

						  GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
						  GTK_STOCK_OK,     GTK_RESPONSE_OK,

						  NULL);

	// main_vbox is the highest hierachy element of the GUI
	main_vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog)->vbox), main_vbox);
	gtk_widget_show (main_vbox);

	// colors frame / vbox holds all element related to color management
	frame_colors = gtk_frame_new (NULL);
	gtk_widget_show (frame_colors);
	gtk_box_pack_start (GTK_BOX (main_vbox), frame_colors, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_colors), 6);

	frame_label_color = gtk_label_new ("Color parameters");
	gtk_widget_show (frame_label_color);
	gtk_frame_set_label_widget (GTK_FRAME (frame_colors), frame_label_color);
	gtk_label_set_use_markup (GTK_LABEL (frame_label_color), TRUE);

	alignment_color = gtk_alignment_new (0.5, 0.5, 1, 1);
	gtk_widget_show (alignment_color);
	gtk_container_add (GTK_CONTAINER (frame_colors), alignment_color);
	gtk_alignment_set_padding (GTK_ALIGNMENT (alignment_color), 6, 6, 6, 6);

	color_opt_vbox = gtk_vbox_new(FALSE, 6);
	gtk_container_add( GTK_CONTAINER (alignment_color), color_opt_vbox);
	gtk_widget_show(color_opt_vbox);

	// export frame / vbox holds all elements related to export options
	frame_export = gtk_frame_new (NULL);
	gtk_widget_show (frame_export);
	gtk_box_pack_start (GTK_BOX (main_vbox), frame_export, TRUE, TRUE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (frame_export), 6);

	frame_label_exp = gtk_label_new ("Export parameters");
	gtk_widget_show (frame_label_exp);
	gtk_frame_set_label_widget (GTK_FRAME (frame_export), frame_label_exp);
	gtk_label_set_use_markup (GTK_LABEL (frame_label_exp), TRUE);

	alignment_exp = gtk_alignment_new (0.5, 0.5, 1, 1);
	gtk_widget_show (alignment_exp);
	gtk_container_add (GTK_CONTAINER (frame_export), alignment_exp);
	gtk_alignment_set_padding (GTK_ALIGNMENT (alignment_exp), 6, 6, 6, 6);

	exp_opt_vbox = gtk_vbox_new(FALSE, 6);
	gtk_container_add( GTK_CONTAINER (alignment_exp), exp_opt_vbox);
	gtk_widget_show(exp_opt_vbox);

	// definition of individual GUI element starts here...
	// the first elements are for color

	// indexer combobox
	hbox_indexer = gtk_hbox_new(FALSE, 6);
	gtk_container_add( GTK_BOX (color_opt_vbox), hbox_indexer);
	gtk_widget_show(hbox_indexer);

	label_indexer = gtk_label_new("Color indexer");
	gtk_widget_show(label_indexer);
	gtk_box_pack_start(GTK_BOX (hbox_indexer), label_indexer, FALSE, FALSE, 6);

	combobox_indexer = gtk_combo_box_text_new();
	gtk_combo_box_text_append_text(combobox_indexer, "Internal");
	gtk_combo_box_text_append_text(combobox_indexer, "MSXize");
	gtk_widget_show(combobox_indexer);
	gtk_box_pack_start(GTK_BOX (hbox_indexer), combobox_indexer, FALSE, FALSE, 6);
	gtk_combo_box_set_active(combobox_indexer, ex_vals.indexer);

	// transparency combobox
	hbox_transparency = gtk_hbox_new(FALSE, 6);
	gtk_container_add( GTK_BOX (color_opt_vbox), hbox_transparency);
	gtk_widget_show(hbox_transparency);

	label_transparency = gtk_label_new("Transparency setting");
	gtk_widget_show(label_transparency);
	gtk_box_pack_start(GTK_BOX (hbox_transparency), label_transparency, FALSE, FALSE, 6);

	combobox_transparency = gtk_combo_box_text_new();
	gtk_combo_box_text_append_text(combobox_transparency, "Use alpha value");
	gtk_combo_box_text_append_text(combobox_transparency, "Use color #ffeb00");
	gtk_combo_box_text_append_text(combobox_transparency, "No transparency");
	gtk_widget_show(combobox_transparency);
	gtk_box_pack_start(GTK_BOX (hbox_transparency), combobox_transparency, FALSE, FALSE, 6);
	gtk_combo_box_set_active(combobox_transparency, ex_vals.transp_setting);

	/*
	checkbox_transparency = gtk_check_button_new_with_mnemonic("Treat #ff00eb as transparent (MSX color 0)");
	gtk_widget_show(checkbox_transparency);
	gtk_box_pack_start (GTK_BOX (color_opt_vbox), checkbox_transparency, FALSE, FALSE, 6);
	gtk_toggle_button_set_active(checkbox_transparency, ex_vals.use_transp);
	*/

	// combobox dither mode
	hbox_dither = gtk_hbox_new(FALSE, 6);
	gtk_container_add( GTK_BOX (color_opt_vbox), hbox_dither);
	gtk_widget_show(hbox_dither);

	label_dither = gtk_label_new("Dither mode");
	gtk_widget_show(label_dither);
	gtk_box_pack_start(GTK_BOX (hbox_dither), label_dither, FALSE, FALSE, 6);

	combobox_dither = gtk_combo_box_text_new();
	gtk_combo_box_text_append_text(combobox_dither, "None");
	gtk_combo_box_text_append_text(combobox_dither, "Floyd-Steinberg (normal)");
	gtk_combo_box_text_append_text(combobox_dither, "Floyd-Steinberg (reduced color bleeding)");
	gtk_combo_box_text_append_text(combobox_dither, "Positioned");
	gtk_widget_show(combobox_dither);
	gtk_box_pack_start(GTK_BOX (hbox_dither), combobox_dither, FALSE, FALSE, 6);
	gtk_combo_box_set_active(combobox_dither, ex_vals.dither_mode);

	/*
	enum_combo_dither = gimp_enum_combo_box_new(GIMP_TYPE_CONVERT_DITHER_TYPE);
	gtk_widget_show(enum_combo_dither);
	gtk_box_pack_start(GTK_BOX (exp_opt_vbox), enum_combo_dither, FALSE, FALSE, 6);
	*/

	// export elements start here
	checkbox_add_bload = gtk_check_button_new_with_mnemonic("Include _BLOAD header");
	gtk_widget_show(checkbox_add_bload);
	gtk_box_pack_start (GTK_BOX (exp_opt_vbox), checkbox_add_bload, FALSE, FALSE, 6);
	gtk_toggle_button_set_active(checkbox_add_bload, ex_vals.add_bload);

	checkbox_check_only = gtk_check_button_new_with_mnemonic("_Check only (don't export)");
	gtk_widget_show(checkbox_check_only);
	gtk_box_pack_start (GTK_BOX (exp_opt_vbox), checkbox_check_only, FALSE, FALSE, 6);
	gtk_toggle_button_set_active(checkbox_check_only, ex_vals.check_only);

	checkbox_compress = gtk_check_button_new_with_mnemonic("Compress image (re-use identical blocks)");
	gtk_widget_show(checkbox_compress);
	gtk_box_pack_start (GTK_BOX (exp_opt_vbox), checkbox_compress, FALSE, FALSE, 6);
	gtk_toggle_button_set_active(checkbox_compress, ex_vals.compress);

	checkbox_split = gtk_check_button_new_with_mnemonic("_Separate file for each screen region");
	gtk_widget_show(checkbox_split);
	gtk_box_pack_start (GTK_BOX (exp_opt_vbox), checkbox_split, FALSE, FALSE, 6);
	gtk_toggle_button_set_active(checkbox_split, ex_vals.split_files);

	checkbox_export_asm = gtk_check_button_new_with_mnemonic("_Export ROM image");
	gtk_widget_show(checkbox_export_asm);
	gtk_box_pack_start (GTK_BOX (exp_opt_vbox), checkbox_export_asm, FALSE, FALSE, 6);
	gtk_toggle_button_set_active(checkbox_export_asm, ex_vals.export_asm);

	hbox_bg_color = gtk_hbox_new(FALSE, 6);
	gtk_container_add( GTK_BOX (exp_opt_vbox), hbox_bg_color);
	gtk_widget_show(hbox_bg_color);

	label_bg_color = gtk_label_new("Back drop color (for ROM image)");
	gtk_widget_show(label_bg_color);
	gtk_box_pack_start(GTK_BOX (hbox_bg_color), label_bg_color, FALSE, FALSE, 6);

	spinner_bg_color = gtk_spin_button_new_with_range(0, 15, 1);
	gtk_widget_show(spinner_bg_color);
	gtk_box_pack_start (GTK_BOX (hbox_bg_color), spinner_bg_color, FALSE, FALSE, 6);
	gtk_spin_button_set_value(spinner_bg_color, ex_vals.bg_color);

	// below are signal connection for the individual GUI elements
	//TODO: gimp_toggle_button_update is deprecated, replace by g_obj_bind_property...

	/*
	g_signal_connect (checkbox_transparency, "toggled",
				  G_CALLBACK (gimp_toggle_button_update),
				  &ex_vals.use_transp);
	*/
	g_signal_connect (checkbox_add_bload, "toggled",
				  G_CALLBACK (gimp_toggle_button_update),
				  &ex_vals.add_bload);
	g_signal_connect (checkbox_check_only, "toggled",
				  G_CALLBACK (gimp_toggle_button_update),
				  &ex_vals.check_only);
	g_signal_connect (checkbox_compress, "toggled",
				  G_CALLBACK (gimp_toggle_button_update),
				  &ex_vals.compress);
	g_signal_connect (checkbox_split, "toggled",
				  G_CALLBACK (gimp_toggle_button_update),
				  &ex_vals.split_files);
	g_signal_connect (checkbox_export_asm, "toggled",
				  G_CALLBACK (gimp_toggle_button_update),
				  &ex_vals.export_asm);
	g_signal_connect (combobox_dither, "changed",
				  G_CALLBACK (read_combo_box),
				  &ex_vals.dither_mode);
	g_signal_connect (combobox_indexer, "changed",
				  G_CALLBACK (read_combo_box),
				  &ex_vals.indexer);
	g_signal_connect (combobox_transparency, "changed",
					  G_CALLBACK (read_combo_box),
					  &ex_vals.transp_setting);
	g_signal_connect (spinner_bg_color, "changed",
					G_CALLBACK (read_spin_button),
					&ex_vals.bg_color);
	/*
	gimp_int_combo_box_connect(enum_combo_dither, GIMP_CONVERT_DITHER_FS_LOWBLEED,
			G_CALLBACK (read_combo_box), &ex_vals.dither_mode);
	*/

	gtk_widget_show (dialog);

	run = (gimp_dialog_run (GIMP_DIALOG (dialog)) == GTK_RESPONSE_OK);

	gtk_widget_destroy (dialog);

	return run;
}

