/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Name of package */
#define PACKAGE "gimp-msx-plugins"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT ""

/* Define to the full name of this package. */
#define PACKAGE_NAME "gimp-msx-plugins"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "gimp-msx-plugins 0.3.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "gimp-msx-plugins"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "0.3.1"

/* Plug-In major version */
#define PLUGIN_MAJOR_VERSION 0

/* Plug-In micro version */
#define PLUGIN_MICRO_VERSION 1

/* Plug-In minor version */
#define PLUGIN_MINOR_VERSION 3

/* Plug-In name */
#define PLUGIN_NAME PACKAGE_NAME

/* Plug-In version */
#define PLUGIN_VERSION PACKAGE_VERSION

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Version number of package */
#define VERSION "0.3.1"
