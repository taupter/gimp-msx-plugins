gimp-msx-plugins
================

.. image:: https://readthedocs.org/projects/gimp-msx-plugins/badge/?version=latest
   :target: https://gimp-msx-plugins.readthedocs.io/en/latest/
   :alt: Documentation Status


*gimp-msx-plugins* is a collection of plug-ins, which provide conversion and 
export capabilities for MSX (1) home computer compatible graphics to the 
GNU Image Manipulation Program (GIMP) GIMP.
The plug-ins included in the collection are:

* MSX GMII Exporter - an image converter to MSX Graphics Mode II / SCREEN 2 compatible 
  format and exporter to binary data files (i.e. VRAM representation of image)
* MSX Sprite Exporter - exports MSX(1) compatible sprites to binary data

Copyright (c) 2019, 2020 Thies Hecker

*gimp-msx-plugins* is released under GPLv3_.

The MSX GMII export is based on the `GIMP Plug-in template`_, Copyright (C) 
2000-2004 Michael Natterer

I have also added support to use the `MSXize plug-in`_ (Copyright (c) Weber 
Estevan Roder Kai, 2019) for the image converter part (needs to be installed 
separately).

For install instructions see INSTALL_.

For the detailed project documentation and user guide see:

/https://gimp-msx-plugins.readthedocs.io/en/latest/

.. _INSTALL: https://gitlab.com/thecker/gimp-msx-plugins/blob/master/INSTALL
.. _GPLv3: https://www.gnu.org/licenses/gpl-3.0.txt
.. _`GIMP Plug-in template`: https://download.gimp.org/pub/gimp/plugin-template/gimp-plugin-template-2.2.0.tar.gz
.. _`MSXize plug-in`: https://github.com/weberkai/msxize
