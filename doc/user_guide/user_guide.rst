User guide
==========

This section explains the usage of the plug-ins.

All plug-ins can be accessed in GIMP via the main menu:

*Filters --> Misc --> MSX...*

Below sections describe the usage of the individual plug-ins.

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   msx_gm2_exporter
   msx_sprite_exporter
