MSX Sprite exporter
===================

The sprite exporter can export (composited) sprites to the MSX(1) sprite pattern
and sprite attribute tables.
Your image dimension have to be multiples of the sprite size (i.e. either 8 or 
16 px). Although the size and number of colors is not limited by the plug-in, 
keep in mind, that the MSX's VDP only supports 32 active (monochrome) sprites. 
And only 4 sprites can be shown per scan line.
The plug-in will raise respective warnings.

Usage
-----

If an image is active, the plug-in can be launched from the *Filters* menu 
(*Filters --> Misc --> MSX sprite exporter*).

A dialog with plug-in option will appear (see explanation of options below).

.. figure:: dialog_sprite_export.png
   
   Sprite exporter options dialog

When you click OK the plug-in will analyze the image and export the sprite 
data. The filenames either end with .sat (for the sprite attribute table) or 
.spt (for the sprite pattern table). Depending on the plug-in options and 
individual sprites in the image one or more .sat and .spt files will be 
exported.

Below example shows a 32x32 pixel 2-color (black and grey) sprite example.

.. figure:: ex02_sprite_gimp.png
   
   32 x 32 px 2-color sprite (with transparent background)
   
Using the default export options the plug-in will export the image into 8 
sprites: 1 sprite for each of the two colors in each of the four 16x16 quarters 
(max. MSX (single) sprite size).
The data will be saved into one .sat and one .spt file containing the attribute 
and pattern information of all 8 sprites.

Plug-in options
---------------

Sprite size
	Can be set to both possible MSX sprite size values - 8 or 16. I.e. either 
	8x8 px or 16x16 px sprites will be extracted.

Use alpha channel for transparent pixels
	If this option is selected, the transparent pixels will be identified by an 
	alpha value of 0 (i.e. transparent like the background in the example above).
	If this option is not selected, transparent pixels have be indicated by a 
	color value of #ff00eb - like shown in the example below:
	
.. figure:: ex02_sprite_gimp_no_transp.png
   
   32 x 32 px 2-color sprite (with transparent pixels indicated by color #ff00eb)

Check only
	If selected, the plug-in does not export any files (just performs the checks)
	
Separate files for each sprite
	If selected individual .sat and .spt files are exported for each sprite. 
	The files will be named ..._01.sat, ...02.sat, etc. - otherwise all sprites 
	are stored in one .sat and one .spt file
	
Export ROM image
	If selected a ROM image with a test program showing the sprite will be 
	exported (additionally to the .sat and .spt files). This can be 
	used for testing the exported output - below image shows the test program 
	for the sprite example above.
 
.. figure:: ex02_sprite_msx.png
   
   openMSX showing the 32 x 32 px 2-color sprite example
   
Background color (for ROM image)
	Defines the background color by one of the 16 MSX color values. 
	(Color 2 - green is used in the example above)

