Developer information
=====================

This section shall give a basic overview for everyone, who would like to help 
improving the *gimp-msx-plugins*.

Submitting bugs and feature requests
------------------------------------

If you observe a bug in the software or if you want to post a feature request, 
please use the issue tracker:

https://gitlab.com/thecker/gimp-msx-plugins/issues
 
The source code
---------------

The source code can be found at:

https://gitlab.com/thecker/gimp-msx-plugins

A brief overview of the repository's folders:

* doc/ - documentation sources (uses sphinx)
* src/ - source code
	* msx_gm2_exporter (written in C)
	* msx_sprite_exporter (written in python)
	* .asm files for ROM image export function op-code
* data/ - palette files required for indexing to MSX color palette

API reference
-------------

TBD

.. the breathe extension for bringing C code via doxygen into sphinx does not 
   run in python2.x and the python code won't compile in sphinx with python3...
   Need to find a better solution here... 


 

