.. This will not build without the gimpfu files (see conf.py)
   Best solution would be to checkout the python sources from the gimp gitlab 
   repo if possible... 

msx_sprite_exporter
===================

Functions in module
-------------------

.. automodule:: msx_sprite_export
   :members:

Classes
-------

SpriteRegion
~~~~~~~~~~~~

.. autoclass:: msx_sprite_export.SpriteRegion
   :members:
   
Sprite
~~~~~~

.. autoclass:: msx_sprite_export.Sprite
   :members:
   
SpriteCollection
~~~~~~~~~~~~~~~~

.. autoclass:: msx_sprite_export.SpriteCollection
   :members: