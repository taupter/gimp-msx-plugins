Introduction
============

*gimp-msx-plugins* is a collection of plug-ins for the GNU Image Manipulation 
Program (GIMP_),which help to create graphics for the MSX (1) home computer.
The plug-ins included in the collection are:

* MSX GMII Exporter - exports background images in MSX Graphics Mode II compatible 
  binary files (pattern, name and color tables)
* MSX Sprite Exporter - exports MSX(1) compatible sprites (binary data for 
  sprite pattern and sprite attribute tables) 

*gimp-msx-plugins* is licensed under GPLv3.

The source code can be found at:

https://gitlab.com/thecker/gimp-msx-plugins

.. _GIMP: http://www.gimp.org/