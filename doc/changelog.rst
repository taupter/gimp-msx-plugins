Change log
==========

release 0.3.1
-------------
Date: 2020-10-03

* Sprite exporter [bugfix]: Disabling "Use alpha for transparent pixels" on images without alpha
  channel crash fixed
* Sprite exporter [bugfix]: Error messages & warnings were not rendered to screen
* GMII exporter: Gimp 2.8 compatibility fix

release 0.3.0
-------------
Date: 2019-11-23

* GMII exporter: automatic image scaling & resizing added 
* GMII exporter: BLOAD headers can now be added using compress & split files option
* GMII exporter: Transparency handling improved (alpha channel now supported)
* GMII exporter: Background color for ROM image can be defined
* palettes adjusted (based on Paul Wratt's HW-MSX palette)

release 0.2.0
-------------
Date: 2019-11-20

* fixed bug #5: sprite exporter raises IOError on Win7
* fixed bug #1/#4: GM II exporter exported wrong colors
* GM II exporter: new color indexing features (select dither type and option to use 
  external converter MSXize by Weber Estevan Roder Kai)

release 0.1.3
-------------
* bugfix in GM II exporter (color identification failed for some blocks)
* sprite exporter added
* GM II exporter some code refactoring

release 0.1.2
-------------
* missing files for configure/make added + system compatibility improved

release 0.1.1
-------------
* fixed bug #2: GM II exporter: compress image does not work properly