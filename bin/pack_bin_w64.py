import os
from shutil import copy, rmtree, move
import subprocess

'''Python script for packaging win64 binary zips'''

# get version string from config.h
with open("../config.h", "r") as config_file:
    config_str = config_file.readlines()

for line in config_str:
    if "#define PACKAGE_VERSION" in line:
        version = line.split(" ")[-1].strip("\"").strip("\"\n")
        break

print("Building package for version", version, "...")
package_name = "gimp-msx-plugins_{}_win64.zip".format(version)

#create tempory dir for package
print("Creating temporary dirs...")
try:
    os.mkdir("temp")
    os.mkdir("temp/plug-ins")
    os.mkdir("temp/palettes")
except FileExistsError:
    print("Dirs already exist...") 

print("Copying files...")
copy("README.txt", "temp/")
copy("../src/msx_gm2_export.exe", "temp/plug-ins/")
copy("../src/msx_sprite_export.py", "temp/plug-ins/")
copy("../data/msx_gm2_no_transp.gpl", "temp/palettes/")
copy("../data/msx_gm2_transp.gpl", "temp/palettes/")

print("Creating zip file...")
top_level_items = ["README.txt", "plug-ins", "palettes"]
os.chdir("temp")
if os.name == 'nt':
    subprocess.run(['tar', '-a',  '-c',  '-f', package_name, *top_level_items])
elif os.name == 'posix':
    subprocess.run(["zip", "-r", package_name, *top_level_items])
else:
    raise ValueError('Unsupported os name \'{}\'!')
os.chdir("..")  #back to original dir

# move package to binary_releases
if not os.path.isdir("../binary_releases"):
    os.mkdir('../binary_releases')
print("Moving package to", os.fspath("../binary_releases"), "...")
move("temp/"+package_name, "../binary_releases/")

# clean up
print("Removing temp folder...")
rmtree("temp")

print("Done.")
